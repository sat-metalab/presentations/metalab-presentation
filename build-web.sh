#!/bin/bash

mkdir -p build

cp presentation.css build/

cp -r Metalab-presentation/includes/img build
mkdir -p build/logos

pushd Metalab-presentation
echo "presentation-metalab.pdf"
pandoc -s --metadata=link-citations:true --citeproc -t html presentation-metalab.tex --css presentation.css -o ../build/fr.html
echo "presentation-metalab-eng.pdf"
pandoc -s --metadata=link-citations:true --citeproc -t html presentation-metalab-eng.tex --css presentation.css -o ../build/en.html
popd

# Fix image paths
sed -i 's|\.\./Metalab\-presentation/includes/||g' build/*.html 
sed -i 's|\.\./metalabcomm/||g' build/*.html 

# Copy and replace eps by png images
eps=`cat build/*.html | grep 'embed src="logos' | sed -re 's/(.*embed src=\")(.*.eps)(\".*)/\2/g' | sort -u`
for e in $eps; do
    l=`echo $e \
        | sed s'|Logo_SAT_abrégé|Logo SAT|' \
        | sed -re s'|logos/SATIE/CMYK/(.*)_CMYK(.*)|logos/SATIE/WEB/\1_RGB\2|' \
        | sed -re s'|logos/Splash/CMYK/(.*)_CMYK(.*)|logos/Splash/WEB/\1_WEB_RGB\-\2|' \
        | sed -re s'|logos/(.*)/CMYK/(.*)|logos/\1/PNG/\2|' \
        | sed -re s'|logos/(.*)/eps/(.*)|logos/\1/png/\2|' \
        | sed s'|\.eps|.png|' \
        `
    d=`dirname $l`
    mkdir -p build/$d
    cp -v "metalabcomm/$l" "build/$l"
    sed -i "s|$e|$l|g" build/*.html 
done

# Remove span artifacts from wrapfigure settings conversion
sed -i "s|<p><span>.*</span> <embed src|<p><embed src|g" build/*.html 

# Generate script for redirecting based on the browser language
echo "<script> \
var language = navigator.language;

if (language.indexOf('fr') > -1) {
    document.location.href = 'fr.html';
} else {
    document.location.href = 'en.html';
}
</script>" > build/index.html
