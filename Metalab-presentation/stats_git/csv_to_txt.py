#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv

parser = argparse.ArgumentParser(
    description='Convert a vsv file into txt file')
parser.add_argument(
    '-f', '--file',
    required=True,
    metavar='',
    help='csv file to convert into txt')

args = parser.parse_args()

with open(args.file + '.txt', "w") as my_output_file:
    with open(args.file, "r") as my_input_file:
        for row in csv.reader(my_input_file):
            my_output_file.write('{:26}'.format(row[0]))
            for i in range(1, len(row)):
                my_output_file.write('{:6}'.format(row[i]))
            my_output_file.write('\n')
    my_output_file.close()
