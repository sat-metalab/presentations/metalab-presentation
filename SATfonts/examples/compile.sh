#!/bin/bash

#quit if one command fails:
set -e

for i in `ls *.tex`; do lualatex $i; done

rm *.log *.aux
