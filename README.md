# Overview
This private repository contains general presentation of the Metalab


# Ubuntu dependencies:

```bash
sudo apt-get install latexmk texlive-bibtex-extra texlive-latex-extra texlive-lang-french texlive-publishers texlive-xetex 
```

# Bibliography and logos

Some documents require the Metalab Bibliography and SAT/Metalab logos, it is included as a submodules of this repository, you can get it with the following command:

```bash
git submodule update --init --recursive
```

# SAT font with lualatex

if you have some errors with fonts you can try to update lualatex font cache with this commands:
```bash
sudo apt install texlive-luatex
luaotfload-tool --update
sudo texhash
```
